module.exports = {
  root: true,
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
  },
  extends: 'airbnb',
  env: {
    node: true,
    es6: true,
  },
  rules: {
    'no-var': 'off',
    'no-console': 'off',
    indent: ['warn', 2],
    'brace-style': ['error', 'stroustrup', { allowSingleLine: true }],
    'object-curly-spacing': ['error', 'never'],
    'object-shorthand': ['warn', 'consistent-as-needed'],
    'comma-dangle': ['warn', 'always-multiline'],
    'no-param-reassign': 'off',
    'react/jsx-uses-vars': 'error',
    'react/prefer-stateless-function': 'off',
    'react/prop-types': ['warn', {skipUndeclared: true}],
  },
};
