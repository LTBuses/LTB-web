const request = require('request');

class WebWatchUpdates {
  /**
   * Creat the update tracker.
   *
   * @param {string} url
   *   The url to the WebWatch UpdateWebMap.aspx endpoint.
   * @param {external:redis} cache
   *   A redis cache client.
   */
  constructor(url, cache) {
    this.cache = cache;
    this.request = request.defaults({url});
  }

  /**
   * Creates a new date object from a date object and a time string.
   *
   * @param {string} dateString
   *   The date.
   * @param {string} timeString
   *   The time as a string. eg. "3:75 PM"
   *
   * @return {Date}
   *   A new date object.
   */
  static fixTimes(dateString, timeString) {
    const date = new Date(dateString);
    const [time, meridiem] = timeString.split(' ');
    const [hours, minutes] = time.split(':');
    date.setMinutes(minutes);
    date.setHours(parseInt(hours, 10) + (meridiem === 'PM' ? 12 : 0));
    return date;
  }

  /**
   * Parses the WebWatch map data into useable data.
   *
   * @param {string} webmap
   *   The webwatch data.
   *
   * @return {Map.<string[]>}
   *   The next three bus times for each stop, keyed by stop number.
   */
  static parse(webmap) {
    const [dateString, majorStops,, minorStops] = webmap.split('*');
    const rawData = (majorStops + minorStops).split(';');
    const data = rawData.reduce((map, stopData) => {
      if (stopData === '') {
        return map;
      }

      const stop = stopData.split('|');
      const stopNumber = parseInt(stop[4].split(' ').pop(), 10);
      const times = stop[5]
        .split('<br>')
        .map(time => this.fixTimes(dateString, time.split('TO').shift().trim()))
        .slice(0, 3);

      map.set(stopNumber, times);

      return map;
    }, new Map());

    return data;
  }

  /**
   * Loads the times for a specific route.
   *
   * @param {number} route
   *   The route number.
   *
   * @return {Map.<string[]>}
   *   The next three bus times for each stop, keyed by stop number.
   */
  async getTimes(route) {
    // return this.request({qs: {u: route}}).pipe(new StreamParser());
    const key = `ltb:times:${route}`;

    const cachedData = await this.cache.get(key).catch(() => null);
    if (cachedData) {
      return new Map(cachedData);
    }

    const data = await new Promise((resolve, reject) => {
      this.request({qs: {u: route}}, (error, response, body) => {
        if (error) {
          reject(error);
          return;
        }
        if (response.statusCode >= 300) {
          reject(new Error(`${response.statusCode}: ${response.statusMessage}`));
          return;
        }
        resolve(WebWatchUpdates.parse(body));
      });
    });

    await this.cache.set(key, Array.from(data), '30s');
    return data;
  }
}

module.exports = WebWatchUpdates;
