const CachemanRedis = require('cacheman-redis');
const Cacheman = require('cacheman');
const redis = require('redis');

const client = redis.createClient(process.env.REDIS_URL || 7777);
const engine = new CachemanRedis(client);
const cache = new Cacheman('ltbuses', {engine});

client.on('error', (...args) => console.error('Error', ...args));
client.on('connect', (...args) => console.log('Connect', ...args));
client.on('ready', (...args) => console.log('ready', ...args));

module.exports = (req, res, next) => {
  req.cache = cache;
  next();
};
