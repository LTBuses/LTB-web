const {createReadStream} = require('fs');
const GtfsData = require('@ltbuses/gtfs-parser/GtfsData');
const GtfsParser = require('@ltbuses/gtfs-parser');
const request = require('request');

const isUrl = path => /^https?:\/\/.*/.test(path);

const getStream = path => (isUrl(path) ? request(path) : createReadStream(path));

const toCache = data => ({
  holidays: Array.from(data.holidays),
  schedules: Array.from(data.schedules),
  stops: Array.from(data.stops),
  routes: Array.from(data.routes),
});

const fromCache = data => ({
  holidays: new Map(data.holidays),
  schedules: new Map(data.schedules),
  stops: new Map(data.stops),
  routes: new Map(data.routes),
});

module.exports = path => async (req, res, next) => {
  const cachedData = await req.cache.get('ltb:data').catch(() => null);

  if (cachedData) {
    req.gtfs = new GtfsData(fromCache(cachedData));
  }
  else {
    const gtfsParser = new GtfsParser(getStream(path));
    req.gtfs = await gtfsParser.getData();

    const rawData = req.gtfs.getRawData();
    await req.cache.set('ltb:data', toCache(rawData), '7 days');
  }

  next();
};
