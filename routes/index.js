const router = require('express').Router();

router.get('/', (req, res) => {
  const search = {
    keyword: (req.query.keyword || '').toLowerCase(),
    route: parseInt(req.query.route, 10) || 'any',
  };

  const stops = req.gtfs.getStops()
    .filter(stop => stop.routes.size > 0)
    .filter((stop) => {
      if (search.route !== 'any' && !stop.routes.has(search.route)) {
        return false;
      }
      return stop.name.toLowerCase().includes(search.keyword);
    })
    .slice(0, 20)
    .map(stop => ({
      number: stop.number,
      name: stop.name.split(' - ').shift(),
      routes: [...stop.routes.keys()],
    }));

  const routes = req.gtfs.getRoutes();

  res.locals.title = 'London Transit Busses';
  res.render('pages/Home', {
    stops,
    routes,
    search,
  });
});

module.exports = router;
