const router = require('express').Router();
const WebWatchUpdates = require('webwatch-updates');

// There is no "/times" page, it's the homepage.
router.get('/', (req, res) => { res.redirect('../'); });

router.get('/:stopNumber', (req, res) => {
  const stopNumber = parseInt(req.params.stopNumber, 10);

  const updates = new WebWatchUpdates(process.env.WEBWATCH_UPDATE_URL, req.cache);

  const stop = req.gtfs.getStops()
    .find(s => parseInt(s.number, 10) === stopNumber);

  res.locals.title = stop.name;
  Promise.all(Array.from(stop.routes.keys()).map(number => updates.getTimes(number)))
    .then(stopTimes => stopTimes.map(list => list.get(stopNumber)))
    .then((stopTimes) => {
      let times = [];
      stop.routes.forEach((name, number) => {
        const routeTimes = stopTimes.shift();
        const output = routeTimes.map(time => ({
          number: number,
          name: name,
          date: new Date(time),
        }));
        times = times.concat(output);
      });
      times = times.sort((t1, t2) => t1.date.getTime() - t2.date.getTime());
      res.render('pages/Times', {times});
    })
    .catch(console.error.bind(console));
});

module.exports = router;
