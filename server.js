/* eslint-disable global-require */

const expressReactViews = require('express-react-views');
const express = require('express');
const path = require('path');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jsx');
app.engine('jsx', expressReactViews.createEngine({beautify: true}));

app.use(express.static('static'));

app.use(require('./middleware/cache'));
app.use(require('./middleware/gtfs')(process.env.GTFS_SOURCE || 'google_transit.zip'));

app.use('/', require('./routes/index'));
app.use('/times', require('./routes/times'));

app.listen(process.env.PORT || 8080, () => {
  console.log('LTC-web listening on port 8080');
});
