module.exports = {
  root: true,
  parserOptions: {},
  extends: 'airbnb/base',
  env: {
    browser: true,
  },
  rules: {
    'no-var': 'off',
    'no-console': 'warn',
    'prefer-arrow-callback': 'off',
    'func-names': 'off',
    indent: ['warn', 2],
    'brace-style': ['error', 'stroustrup', { allowSingleLine: true }],
    'object-curly-spacing': ['error', 'never'],
    'object-shorthand': ['warn', 'consistent-as-needed'],
    'comma-dangle': ['warn', 'always-multiline'],
    'no-param-reassign': 'off',
  },
};
