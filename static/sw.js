/* eslint-env serviceworker */

// eslint-disable-next-line no-restricted-globals
var sw = self;

var cacheName = 'ltbuses-v1';
var urlsToCache = [
  '/',
  '/styles.css',
  '/js/main.js',
];

sw.addEventListener('install', function (event) {
  var preCache = caches.open(cacheName)
    .then(function (cache) {
      return cache.addAll(urlsToCache);
    });

  event.waitUntil(preCache);
});

sw.addEventListener('fetch', function (event) {
  var result = caches.match(event.request)
    .then(function (response) {
      return response || fetch(event.request);
    });
  event.respondWith(result);
});
