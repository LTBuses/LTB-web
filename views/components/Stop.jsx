const React = require('react');

class Stop extends React.Component {
  render() {
    return (
      <a href={`/times/${this.props.number}`} className="stop">
        <h3>
          {this.props.name}
        </h3>
        <div className="stop-number">Stop {this.props.number}</div>
        <div className="stop-routes">
          <h4>Busses</h4>
          <ul>
            {this.props.routes.map(route => (
              <li key={route}>{route}</li>
            ))}
          </ul>
        </div>
      </a>
    );
  }
}

module.exports = Stop;
