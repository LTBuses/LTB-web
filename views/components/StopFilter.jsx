const React = require('react');

class RouteFilter extends React.Component {
  render() {
    const {id, keyword, route} = this.props;
    return (
      <form action="" className="filter" method="get">
        <div className="formItem">
          <label htmlFor={`${id}-keyword`}>Search</label>
          <input
            type="search"
            size="10"
            id={`${id}-keyword`}
            name="keyword"
            defaultValue={keyword}
          />
        </div>
        <div className="formItem">
          <label htmlFor={`${id}-route`}>Route</label>
          <select id={`${id}-router`} name="route" defaultValue={route}>
            <option value="">Any Route</option>
            {this.props.routes.map(r => (
              <option key={r.number} value={r.number}>
                {r.name}
              </option>
            ))}
          </select>
        </div>
        <div className="formItem">
          <input type="submit" value="Search" />
        </div>
      </form>
    );
  }
}

module.exports = RouteFilter;
