const React = require('react');

class StopList extends React.Component {
  render() {
    return (
      <div className="stopList">
        {this.props.children}
      </div>
    );
  }
}

module.exports = StopList;
