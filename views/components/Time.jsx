const moment = require('moment');
const React = require('react');

class Time extends React.Component {
  render() {
    const time = moment(this.props.date);
    return (
      <a href={`/times/${this.props.number}`} className="stop">
        <h3>
          {this.props.name}
        </h3>
        <div className="stop-number">{this.props.number}</div>
        <div className="stop-routes">
          <h4>Time</h4>
          <ul>
            <li>{time.format('h:mm a')} ({time.fromNow()})</li>
          </ul>
        </div>
      </a>
    );
  }
}

module.exports = Time;
