const React = require('react');

class DefaultLayout extends React.Component {
  render() {
    return (
      <html lang="en">
        <head>
          <title>{this.props.title} | LTB Web</title>
          <meta charSet="utf-8" />
          <link rel="stylesheet" href="/styles.css" />
          <link rel="manifest" href="/manifest.json" />
          <script type="text/javascript" src="/js/main.js" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <meta name="theme-color" content="#0073c6" />
        </head>
        <body>
          <header>
            <h1>{this.props.title}</h1>
          </header>
          <main role="main">
            {this.props.children}
          </main>
        </body>
      </html>
    );
  }
}

module.exports = DefaultLayout;
