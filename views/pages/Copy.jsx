const React = require('react');
const DefaultLayout = require('../layouts/DefaultLayout.jsx');

class Copy extends React.Component {
  render() {
    return (
      <DefaultLayout title={this.props.title} nav={this.props.nav}>
        {this.props.copy}
      </DefaultLayout>
    );
  }
}

module.exports = Copy;
