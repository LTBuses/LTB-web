const React = require('react');
const DefaultLayout = require('../layouts/DefaultLayout.jsx');
const StopFilter = require('../components/StopFilter.jsx');
const StopList = require('../components/StopList.jsx');
const Stop = require('../components/Stop.jsx');

class Home extends React.Component {
  render() {
    return (
      <DefaultLayout title={this.props.title} nav={this.props.nav}>
        <StopFilter
          id="search"
          keyword={this.props.search.keyword}
          route={this.props.search.route}
          routes={this.props.routes}
        />
        <StopList>
          {this.props.stops.map(stop => (
            <Stop
              key={stop.number}
              name={stop.name}
              number={stop.number}
              routes={stop.routes}
            />
          ))}
        </StopList>
      </DefaultLayout>
    );
  }
}

module.exports = Home;
