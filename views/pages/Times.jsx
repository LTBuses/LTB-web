const React = require('react');
const DefaultLayout = require('../layouts/DefaultLayout.jsx');
const StopList = require('../components/StopList.jsx');
const Time = require('../components/Time.jsx');

class Home extends React.Component {
  render() {
    return (
      <DefaultLayout title={this.props.title} nav={this.props.nav}>
        <StopList>
          {this.props.times.map(time => (
            <Time
              key={`${time.number}-${time.date.getTime()}`}
              name={time.name}
              number={time.number}
              date={time.date}
            />
          ))}
        </StopList>
      </DefaultLayout>
    );
  }
}

module.exports = Home;
